var subscribeOverlaySubscribeButton = document.getElementById("subscribe");

function show_subscribe_overlay() {
  subscribeOverlaySubscribeButton.style.display = "flex";
}

function hide_subscribe_overlay() {
  subscribeOverlaySubscribeButton.style.display = "none";
}