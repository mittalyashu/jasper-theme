# Jasper Theme

[![Star on GitLab][img-gitlab]][gitlab] [![Buy me a coffee][img-buymeacoffee]][buymeacoffee] [![Chat on Discord][img-discord]][discord]

Jasper Theme is a gem based theme.

## Copyright & License

Design of the [Jasper theme][gitlab] is based on the [Casper theme][casper] by Ghost team.

The starter kit is available as open source under the terms of the [MIT License][gitlab-license].

## Help me! To manage this project

I've put a lot of time and effort into making **Jasper** project. If you love it, you can buy me a coffee. I promise it will be a good investment 😉.

**Donate with:** [Buy me a coffee][buymeacoffee].

<!-- Links -->
[gitlab]: https://gitlab.com/mittalyashu/jasper
[gitlab-license]: https://gitlab.com/mittalyashu/jasper/blob/master/LICENSE
[casper]: https://github.com/TryGhost/Casper/blob/master/LICENSE
[discord]: https://discord.gg/JKpSUQQ
[buymeacoffee]: https://www.buymeacoffee.com/mittalyashu

<!-- Images -->
[img-jasper]: https://i.imgur.com/RZ1H9Id.png
[img-discord]: https://img.shields.io/badge/chat-on%20discord-blue.svg?style=flat&colorA=555555&colorB=7289DA
[img-buymeacoffee]: https://img.shields.io/badge/buy-me%20a%20coffee-blue.svg?style=flat&colorA=555555&colorB=FF813F
[img-twitter]: https://img.shields.io/badge/share-on%20twitter-blue.svg?style=flat&colorA=555555&colorB=1DA1F2
[img-gitlab]: https://img.shields.io/badge/star-on%20gitlab-yellow.svg?style=flat&colorA=555555&colorB=554488