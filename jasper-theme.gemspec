# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jasper-theme"
	spec.version       = "0.0.2"
  spec.authors       = ["Yashu Mittal"]
  spec.email         = ["yashu@codecarrot.net"]

  spec.summary       = "A full-featured customisable theme Casper 👻"
  spec.homepage      = "https://gitlab.com/mittalyashu/jasper"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
